// Javascript for js-slider
// Only MDN Javacript documentation, no library

(function(){

  function slider(selector){
    // container / list / items
    var el = document.querySelector(selector),
        ul = el.querySelector('ul'),
        items = ul.querySelectorAll('li'),
        activeClass = 'active',
        nbItemsDisplay = 2,
        currentItem = 0,
        elHeight,
        intervalX,
        margin = 30;


    // If number of items is inferiro to number item displaying
    if (items.length <= nbItemsDisplay) {
      return false;
    }

    // Position absolute for items container 
    ul.style.position = 'relative';
    ul.style.width = (items.length * (100 / nbItemsDisplay)) + '%';

    elHeight = items[0].offsetHeight,
    wrapper = document.createElement('div');
    
    wrapper.classList.add('js-slider-wrapper');
    wrapper.appendChild(ul);
    el.appendChild(wrapper);
    
    el.style.position = 'relative';
    el.style.height = elHeight + 'px';
    
    // intervalX (15px for margin items)
    items.forEach( function(item){
      item.style.width = 'calc(' + (100/nbItemsDisplay) + '% - ' + (margin) + 'px)';
    });
    intervalX = items[1].offsetWidth + margin;
    console.log(intervalX);
  
    // Create Nav buttons  
    var nav = document.createElement('div');
    var btnPrev = document.createElement('span');
    var btnNext = document.createElement('span');
    nav.classList.add('js-slider-nav');
    btnPrev.classList.add('js-slider-btn', 'prev');
    btnPrev.textContent = '<';
    btnPrev.setAttribute('href', "#");
    btnNext.classList.add('js-slider-btn', 'next');
    btnNext.textContent = '>';
    btnNext.setAttribute('href', "#");

    nav.appendChild(btnPrev);
    nav.appendChild(btnNext);
    el.appendChild(nav);
    
    
    // Init
    ul.style.left = '0px';
    statusSlides(items);


    // Event
    btnNext.addEventListener('click', function(e){
      moveSlider(e, ul, items, 'right')
    });
    btnPrev.addEventListener('click', function(e){
      moveSlider(e, ul, items, 'left')
    });
    window.addEventListener('resize', function(e){
      elHeight = items[0].offsetHeight;
      el.style.height = elHeight + 'px';
    });

    //  Move List on X axis
    function moveSlider(event, el, items, orientation ){
      if (event.target.classList.contains('disable')){
        return false;
      }

      var currentLeft = parseInt(ul.style.left);
  
      if( orientation === 'right'){
        currentItem++;
        el.style.left = (currentLeft - intervalX) + 'px';
      } else {
        currentItem--;
        el.style.left = (currentLeft + intervalX) + 'px';     
      }
      statusSlides(items);
    }

    function statusNav(items){
      nbItem = items.length;
      firstItem = items[0];
      lastItem = items[nbItem - 1];      
      if (firstItem.classList.contains(activeClass)){
        btnPrev.classList.add('disable');
        btnNext.classList.remove('disable');
      } else if(lastItem.classList.contains(activeClass)) {
        btnNext.classList.add('disable');
        btnPrev.classList.remove('disable');
      } else {
        btnNext.classList.remove('disable');
        btnPrev.classList.remove('disable');        
      }
    }

    function statusSlides(items){
      items.forEach( function(item){
        item.classList.remove(activeClass);
      });
      items[currentItem].classList.add(activeClass);
      items[currentItem + 1].classList.add(activeClass);
      statusNav(items);
    }    
  };
  
  //window.addEventListener('DOMContentLoaded',function(e){slider('.js-slider')});
  window.addEventListener('load',function(e){slider('.js-slider')});

})();
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm1haW4uanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwiZmlsZSI6ImFsbC5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8vIEphdmFzY3JpcHQgZm9yIGpzLXNsaWRlclxuLy8gT25seSBNRE4gSmF2YWNyaXB0IGRvY3VtZW50YXRpb24sIG5vIGxpYnJhcnlcblxuKGZ1bmN0aW9uKCl7XG5cbiAgZnVuY3Rpb24gc2xpZGVyKHNlbGVjdG9yKXtcbiAgICAvLyBjb250YWluZXIgLyBsaXN0IC8gaXRlbXNcbiAgICB2YXIgZWwgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKHNlbGVjdG9yKSxcbiAgICAgICAgdWwgPSBlbC5xdWVyeVNlbGVjdG9yKCd1bCcpLFxuICAgICAgICBpdGVtcyA9IHVsLnF1ZXJ5U2VsZWN0b3JBbGwoJ2xpJyksXG4gICAgICAgIGFjdGl2ZUNsYXNzID0gJ2FjdGl2ZScsXG4gICAgICAgIG5iSXRlbXNEaXNwbGF5ID0gMixcbiAgICAgICAgY3VycmVudEl0ZW0gPSAwLFxuICAgICAgICBlbEhlaWdodCxcbiAgICAgICAgaW50ZXJ2YWxYLFxuICAgICAgICBtYXJnaW4gPSAzMDtcblxuXG4gICAgLy8gSWYgbnVtYmVyIG9mIGl0ZW1zIGlzIGluZmVyaXJvIHRvIG51bWJlciBpdGVtIGRpc3BsYXlpbmdcbiAgICBpZiAoaXRlbXMubGVuZ3RoIDw9IG5iSXRlbXNEaXNwbGF5KSB7XG4gICAgICByZXR1cm4gZmFsc2U7XG4gICAgfVxuXG4gICAgLy8gUG9zaXRpb24gYWJzb2x1dGUgZm9yIGl0ZW1zIGNvbnRhaW5lciBcbiAgICB1bC5zdHlsZS5wb3NpdGlvbiA9ICdyZWxhdGl2ZSc7XG4gICAgdWwuc3R5bGUud2lkdGggPSAoaXRlbXMubGVuZ3RoICogKDEwMCAvIG5iSXRlbXNEaXNwbGF5KSkgKyAnJSc7XG5cbiAgICBlbEhlaWdodCA9IGl0ZW1zWzBdLm9mZnNldEhlaWdodCxcbiAgICB3cmFwcGVyID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnZGl2Jyk7XG4gICAgXG4gICAgd3JhcHBlci5jbGFzc0xpc3QuYWRkKCdqcy1zbGlkZXItd3JhcHBlcicpO1xuICAgIHdyYXBwZXIuYXBwZW5kQ2hpbGQodWwpO1xuICAgIGVsLmFwcGVuZENoaWxkKHdyYXBwZXIpO1xuICAgIFxuICAgIGVsLnN0eWxlLnBvc2l0aW9uID0gJ3JlbGF0aXZlJztcbiAgICBlbC5zdHlsZS5oZWlnaHQgPSBlbEhlaWdodCArICdweCc7XG4gICAgXG4gICAgLy8gaW50ZXJ2YWxYICgxNXB4IGZvciBtYXJnaW4gaXRlbXMpXG4gICAgaXRlbXMuZm9yRWFjaCggZnVuY3Rpb24oaXRlbSl7XG4gICAgICBpdGVtLnN0eWxlLndpZHRoID0gJ2NhbGMoJyArICgxMDAvbmJJdGVtc0Rpc3BsYXkpICsgJyUgLSAnICsgKG1hcmdpbikgKyAncHgpJztcbiAgICB9KTtcbiAgICBpbnRlcnZhbFggPSBpdGVtc1sxXS5vZmZzZXRXaWR0aCArIG1hcmdpbjtcbiAgICBjb25zb2xlLmxvZyhpbnRlcnZhbFgpO1xuICBcbiAgICAvLyBDcmVhdGUgTmF2IGJ1dHRvbnMgIFxuICAgIHZhciBuYXYgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdkaXYnKTtcbiAgICB2YXIgYnRuUHJldiA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ3NwYW4nKTtcbiAgICB2YXIgYnRuTmV4dCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ3NwYW4nKTtcbiAgICBuYXYuY2xhc3NMaXN0LmFkZCgnanMtc2xpZGVyLW5hdicpO1xuICAgIGJ0blByZXYuY2xhc3NMaXN0LmFkZCgnanMtc2xpZGVyLWJ0bicsICdwcmV2Jyk7XG4gICAgYnRuUHJldi50ZXh0Q29udGVudCA9ICc8JztcbiAgICBidG5QcmV2LnNldEF0dHJpYnV0ZSgnaHJlZicsIFwiI1wiKTtcbiAgICBidG5OZXh0LmNsYXNzTGlzdC5hZGQoJ2pzLXNsaWRlci1idG4nLCAnbmV4dCcpO1xuICAgIGJ0bk5leHQudGV4dENvbnRlbnQgPSAnPic7XG4gICAgYnRuTmV4dC5zZXRBdHRyaWJ1dGUoJ2hyZWYnLCBcIiNcIik7XG5cbiAgICBuYXYuYXBwZW5kQ2hpbGQoYnRuUHJldik7XG4gICAgbmF2LmFwcGVuZENoaWxkKGJ0bk5leHQpO1xuICAgIGVsLmFwcGVuZENoaWxkKG5hdik7XG4gICAgXG4gICAgXG4gICAgLy8gSW5pdFxuICAgIHVsLnN0eWxlLmxlZnQgPSAnMHB4JztcbiAgICBzdGF0dXNTbGlkZXMoaXRlbXMpO1xuXG5cbiAgICAvLyBFdmVudFxuICAgIGJ0bk5leHQuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCBmdW5jdGlvbihlKXtcbiAgICAgIG1vdmVTbGlkZXIoZSwgdWwsIGl0ZW1zLCAncmlnaHQnKVxuICAgIH0pO1xuICAgIGJ0blByZXYuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCBmdW5jdGlvbihlKXtcbiAgICAgIG1vdmVTbGlkZXIoZSwgdWwsIGl0ZW1zLCAnbGVmdCcpXG4gICAgfSk7XG4gICAgd2luZG93LmFkZEV2ZW50TGlzdGVuZXIoJ3Jlc2l6ZScsIGZ1bmN0aW9uKGUpe1xuICAgICAgZWxIZWlnaHQgPSBpdGVtc1swXS5vZmZzZXRIZWlnaHQ7XG4gICAgICBlbC5zdHlsZS5oZWlnaHQgPSBlbEhlaWdodCArICdweCc7XG4gICAgfSk7XG5cbiAgICAvLyAgTW92ZSBMaXN0IG9uIFggYXhpc1xuICAgIGZ1bmN0aW9uIG1vdmVTbGlkZXIoZXZlbnQsIGVsLCBpdGVtcywgb3JpZW50YXRpb24gKXtcbiAgICAgIGlmIChldmVudC50YXJnZXQuY2xhc3NMaXN0LmNvbnRhaW5zKCdkaXNhYmxlJykpe1xuICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICB9XG5cbiAgICAgIHZhciBjdXJyZW50TGVmdCA9IHBhcnNlSW50KHVsLnN0eWxlLmxlZnQpO1xuICBcbiAgICAgIGlmKCBvcmllbnRhdGlvbiA9PT0gJ3JpZ2h0Jyl7XG4gICAgICAgIGN1cnJlbnRJdGVtKys7XG4gICAgICAgIGVsLnN0eWxlLmxlZnQgPSAoY3VycmVudExlZnQgLSBpbnRlcnZhbFgpICsgJ3B4JztcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGN1cnJlbnRJdGVtLS07XG4gICAgICAgIGVsLnN0eWxlLmxlZnQgPSAoY3VycmVudExlZnQgKyBpbnRlcnZhbFgpICsgJ3B4JzsgICAgIFxuICAgICAgfVxuICAgICAgc3RhdHVzU2xpZGVzKGl0ZW1zKTtcbiAgICB9XG5cbiAgICBmdW5jdGlvbiBzdGF0dXNOYXYoaXRlbXMpe1xuICAgICAgbmJJdGVtID0gaXRlbXMubGVuZ3RoO1xuICAgICAgZmlyc3RJdGVtID0gaXRlbXNbMF07XG4gICAgICBsYXN0SXRlbSA9IGl0ZW1zW25iSXRlbSAtIDFdOyAgICAgIFxuICAgICAgaWYgKGZpcnN0SXRlbS5jbGFzc0xpc3QuY29udGFpbnMoYWN0aXZlQ2xhc3MpKXtcbiAgICAgICAgYnRuUHJldi5jbGFzc0xpc3QuYWRkKCdkaXNhYmxlJyk7XG4gICAgICAgIGJ0bk5leHQuY2xhc3NMaXN0LnJlbW92ZSgnZGlzYWJsZScpO1xuICAgICAgfSBlbHNlIGlmKGxhc3RJdGVtLmNsYXNzTGlzdC5jb250YWlucyhhY3RpdmVDbGFzcykpIHtcbiAgICAgICAgYnRuTmV4dC5jbGFzc0xpc3QuYWRkKCdkaXNhYmxlJyk7XG4gICAgICAgIGJ0blByZXYuY2xhc3NMaXN0LnJlbW92ZSgnZGlzYWJsZScpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgYnRuTmV4dC5jbGFzc0xpc3QucmVtb3ZlKCdkaXNhYmxlJyk7XG4gICAgICAgIGJ0blByZXYuY2xhc3NMaXN0LnJlbW92ZSgnZGlzYWJsZScpOyAgICAgICAgXG4gICAgICB9XG4gICAgfVxuXG4gICAgZnVuY3Rpb24gc3RhdHVzU2xpZGVzKGl0ZW1zKXtcbiAgICAgIGl0ZW1zLmZvckVhY2goIGZ1bmN0aW9uKGl0ZW0pe1xuICAgICAgICBpdGVtLmNsYXNzTGlzdC5yZW1vdmUoYWN0aXZlQ2xhc3MpO1xuICAgICAgfSk7XG4gICAgICBpdGVtc1tjdXJyZW50SXRlbV0uY2xhc3NMaXN0LmFkZChhY3RpdmVDbGFzcyk7XG4gICAgICBpdGVtc1tjdXJyZW50SXRlbSArIDFdLmNsYXNzTGlzdC5hZGQoYWN0aXZlQ2xhc3MpO1xuICAgICAgc3RhdHVzTmF2KGl0ZW1zKTtcbiAgICB9ICAgIFxuICB9O1xuICBcbiAgLy93aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcignRE9NQ29udGVudExvYWRlZCcsZnVuY3Rpb24oZSl7c2xpZGVyKCcuanMtc2xpZGVyJyl9KTtcbiAgd2luZG93LmFkZEV2ZW50TGlzdGVuZXIoJ2xvYWQnLGZ1bmN0aW9uKGUpe3NsaWRlcignLmpzLXNsaWRlcicpfSk7XG5cbn0pKCk7Il19
