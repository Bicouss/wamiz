// Javascript for js-slider
// Only MDN Javacript documentation, no library

(function(){

  function slider(selector){
    // container / list / items
    var el = document.querySelector(selector),
        ul = el.querySelector('ul'),
        items = ul.querySelectorAll('li'),
        activeClass = 'active',
        nbItemsDisplay = 2,
        currentItem = 0,
        elHeight,
        intervalX,
        margin = 30;


    // If number of items is inferiro to number item displaying
    if (items.length <= nbItemsDisplay) {
      return false;
    }

    // Position absolute for items container 
    ul.style.position = 'relative';
    ul.style.width = (items.length * (100 / nbItemsDisplay)) + '%';

    elHeight = items[0].offsetHeight,
    wrapper = document.createElement('div');
    
    wrapper.classList.add('js-slider-wrapper');
    wrapper.appendChild(ul);
    el.appendChild(wrapper);
    
    el.style.position = 'relative';
    el.style.height = elHeight + 'px';
    
    // intervalX (15px for margin items)
    items.forEach( function(item){
      item.style.width = 'calc(' + (100/nbItemsDisplay) + '% - ' + (margin) + 'px)';
    });
    intervalX = items[1].offsetWidth + margin;
    console.log(intervalX);
  
    // Create Nav buttons  
    var nav = document.createElement('div');
    var btnPrev = document.createElement('span');
    var btnNext = document.createElement('span');
    nav.classList.add('js-slider-nav');
    btnPrev.classList.add('js-slider-btn', 'prev');
    btnPrev.textContent = '<';
    btnPrev.setAttribute('href', "#");
    btnNext.classList.add('js-slider-btn', 'next');
    btnNext.textContent = '>';
    btnNext.setAttribute('href', "#");

    nav.appendChild(btnPrev);
    nav.appendChild(btnNext);
    el.appendChild(nav);
    
    
    // Init
    ul.style.left = '0px';
    statusSlides(items);


    // Event
    btnNext.addEventListener('click', function(e){
      moveSlider(e, ul, items, 'right')
    });
    btnPrev.addEventListener('click', function(e){
      moveSlider(e, ul, items, 'left')
    });
    window.addEventListener('resize', function(e){
      elHeight = items[0].offsetHeight;
      el.style.height = elHeight + 'px';
    });

    //  Move List on X axis
    function moveSlider(event, el, items, orientation ){
      if (event.target.classList.contains('disable')){
        return false;
      }

      var currentLeft = parseInt(ul.style.left);
  
      if( orientation === 'right'){
        currentItem++;
        el.style.left = (currentLeft - intervalX) + 'px';
      } else {
        currentItem--;
        el.style.left = (currentLeft + intervalX) + 'px';     
      }
      statusSlides(items);
    }

    function statusNav(items){
      nbItem = items.length;
      firstItem = items[0];
      lastItem = items[nbItem - 1];      
      if (firstItem.classList.contains(activeClass)){
        btnPrev.classList.add('disable');
        btnNext.classList.remove('disable');
      } else if(lastItem.classList.contains(activeClass)) {
        btnNext.classList.add('disable');
        btnPrev.classList.remove('disable');
      } else {
        btnNext.classList.remove('disable');
        btnPrev.classList.remove('disable');        
      }
    }

    function statusSlides(items){
      items.forEach( function(item){
        item.classList.remove(activeClass);
      });
      items[currentItem].classList.add(activeClass);
      items[currentItem + 1].classList.add(activeClass);
      statusNav(items);
    }    
  };

  window.addEventListener('load',function(e){slider('.js-slider')});

})();